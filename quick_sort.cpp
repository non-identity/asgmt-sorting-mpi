#include <unistd.h>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include "timer.hpp"

void LoadArrayFromFile(int *arr, int elements_count, std::ifstream &data_file) {
    for (int i = 0; i < elements_count; i++) {
        data_file >> arr[i];
    }
}

int CompareInts(const void *left, const void *right) {
    if (*(int *)left < *(int *)right) return -1;
    if (*(int *)left == *(int *)right) return 0;
    return 1;
}

int main(int argc, char **argv) {
    std::string input_file_name, result_file_name, time_file_name;
    int num_elements;
    int cmd_flag;
    while ((cmd_flag = getopt(argc, argv, "n:i:r:t:")) != -1) {
        switch (cmd_flag) {
            case 'n':
                num_elements = std::stoi(std::string(optarg));
                break;
            case 'i':
                input_file_name.assign(optarg);
                break;
            case 'r':
                result_file_name.assign(optarg);
                break;
            case 't':
                time_file_name.assign(optarg);
                break;
            default:
                std::cerr << "Invalid command line argument" << std::endl;
                exit(1);
        }
    }
    std::ifstream input_file(input_file_name);
    std::ofstream result_file(result_file_name), time_file(time_file_name);
    if (!input_file || !result_file || !time_file) {
        std::cerr << "Error opening one or more files" << std::endl;
        exit(1);
    }
    int *arr = new int[num_elements];
    LoadArrayFromFile(arr, num_elements, input_file);
    Timer timer;
    std::qsort(arr, num_elements, sizeof(int), CompareInts);
    time_file << timer.GetElapsedTime() << std::endl;
    for (int i = 0; i < num_elements; i++) {
        result_file << arr[i] << std::endl;
    }
    return 0;
}
