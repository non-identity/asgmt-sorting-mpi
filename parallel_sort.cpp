#include <mpi.h>
#include <unistd.h>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <limits>
#include "timer.hpp"

void LoadArrayFromFile(int *arr, int elements_count, std::ifstream &data_file) {
    for (int i = 0; i < elements_count; i++) {
        data_file >> arr[i];
    }
}

int CompareInts(const void *left, const void *right) {
    if (*(int *)left < *(int *)right) return -1;
    if (*(int *)left == *(int *)right) return 0;
    return 1;
}

int *Merge(int *arr_1, int *arr_2, int count_1, int count_2) {
    int *result_arr = new int[count_1 + count_2];
    int i = 0, j = 0, k = 0;
    while (i < count_1 && j < count_2) {
        if (arr_1[i] < arr_2[j]) {
            result_arr[k++] = arr_1[i++];
        } else {
            result_arr[k++] = arr_2[j++];
        }
    }
    while (i < count_1) {
        result_arr[k++] = arr_1[i++];
    }
    while (j < count_2) {
        result_arr[k++] = arr_2[j++];
    }
    return result_arr;
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);
    int rank, world_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int *arr;
    int num_data, num_elements;
    int *local_arr;
    int local_num_elements;
    std::ofstream result_file, time_file;
    if (rank == 0) {
        std::string input_file_name, result_file_name, time_file_name;
        int cmd_flag;
        while ((cmd_flag = getopt(argc, argv, "n:i:r:t:")) != -1) {
            switch (cmd_flag) {
                case 'n':
                    num_data = std::stoi(std::string(optarg));
                    break;
                case 'i':
                    input_file_name.assign(optarg);
                    break;
                case 'r':
                    result_file_name.assign(optarg);
                    break;
                case 't':
                    time_file_name.assign(optarg);
                    break;
                default:
                    std::cerr << "Invalid command line argument" << std::endl;
                    MPI_Abort(MPI_COMM_WORLD, 1);
            }
        }
        std::ifstream input_file(input_file_name);
        result_file.open(result_file_name);
        time_file.open(time_file_name);
        if (!input_file || !result_file || !time_file) {
            std::cerr << "Error opening one or more files" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        int padding_needed =
            (world_size - (num_data % world_size)) % world_size;
        arr = new int[num_data + padding_needed];
        LoadArrayFromFile(arr, num_data, input_file);
        for (int i = 0; i < padding_needed; i++) {
            arr[num_data + i] = std::numeric_limits<int>::max();
        }
        num_elements = num_data + padding_needed;
    }
    MPI_Bcast(&num_elements, 1, MPI_INT, 0, MPI_COMM_WORLD);

    Timer timer;
    local_num_elements = num_elements / world_size;
    local_arr = new int[local_num_elements];
    MPI_Scatter(arr, local_num_elements, MPI_INT, local_arr, local_num_elements,
                MPI_INT, 0, MPI_COMM_WORLD);

    std::qsort(local_arr, local_num_elements, sizeof(int), CompareInts);

    for (int jump_size = 1; jump_size < world_size; jump_size *= 2) {
        if (rank % jump_size == 0) {
            if ((rank / jump_size) % 2 == 0) {
                int sender_rank = rank + jump_size;
                if (sender_rank < world_size) {
                    int *recv_arr = new int[local_num_elements];
                    MPI_Status status;
                    MPI_Recv(recv_arr, local_num_elements, MPI_INT, sender_rank,
                             jump_size, MPI_COMM_WORLD, &status);
                    int recv_count;
                    MPI_Get_count(&status, MPI_INT, &recv_count);
                    int *new_arr = Merge(local_arr, recv_arr,
                                         local_num_elements, recv_count);
                    int *tmp = local_arr;
                    local_arr = new_arr;
                    local_num_elements += recv_count;
                    delete tmp;
                    delete recv_arr;
                }
            } else {
                int recvr_rank = rank - jump_size;
                MPI_Send(local_arr, local_num_elements, MPI_INT, recvr_rank,
                         jump_size, MPI_COMM_WORLD);
                delete local_arr;
            }
        }
    }

    time_file << timer.GetElapsedTime() << std::endl;

    if (rank == 0) {
        for (int i = 0; i < num_data; i++) {
            result_file << local_arr[i] << std::endl;
        }
    }

    MPI_Finalize();
    return 0;
}
