import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

SEQ_TIME_FILE_FORMAT = "output/time_%d.txt"
MPI_TIME_FILE_FORMAT = "output/time_mpi_%d_%d.txt"

PROCESS_COUNT = [4, 8, 16, 32, 64, 80]

TIME_PLOT_PATH = "results/times.png"
SPEEDUP_PLOT_PATH = "results/speedup.png"


def get_time_from_file(file_path):
    with open(file_path) as f:
        return int(next(f))


if __name__ == "__main__":
    sum = 0
    for i in range(1, 6):
        sum += get_time_from_file(SEQ_TIME_FILE_FORMAT % i)
    seq_avg_time = sum / 5
    print("Avg time for sequential sort(ms): %f" % seq_avg_time)
    avg_time = []
    for p in PROCESS_COUNT:
        sum = 0
        for i in range(1, 6):
            sum += get_time_from_file(MPI_TIME_FILE_FORMAT % (p, i))
        avg_time.append(sum / 5)
        print("Avg time for parallel sort using %d processors(ms): %f" %
              (p, avg_time[-1]))
    plt.figure()
    plt.plot(PROCESS_COUNT, avg_time, '-b', label="Parallel Merge Sort")
    plt.axhline(
        y=seq_avg_time,
        color='r',
        linestyle='--',
        label="Sequential Quick Sort (independent of X-axis)")
    plt.legend(loc="upper left")
    plt.title("Running time vs Process Count")
    plt.xlabel("Number of processes")
    plt.ylabel("Time taken (milliSeconds)")
    plt.savefig(TIME_PLOT_PATH)
    speedups = [seq_avg_time / x for x in avg_time]
    plt.figure()
    plt.plot(
        PROCESS_COUNT, speedups, '-b', label="Speedup of Parallel Merge Sort")
    plt.legend(loc="upper right")
    plt.title("Speedup vs Process Count")
    plt.xlabel("Number of processes")
    plt.ylabel("Speedup")
    plt.savefig(SPEEDUP_PLOT_PATH)
